<?php

namespace C4U\Acl;

use Nette\Security\IAuthenticator;
use Nette\Security\AuthenticationException;
use Nette\Security\Identity;
use dibi;

class FacebookAuthenticator {

	private $connection;

	public function __construct(\DibiConnection $dibi) {
		$this->connection = $dibi;
	}

    /*
     * Performs an authentication
     * @param array
     * @return void
     * @throws AuthenticationException
     */
	public function authenticate(array $data) {
		$username = $data['email'];

		$row = $this->connection->select('*')
			 ->from('[' . AclModel::USERS_TABLE . ']')
			 ->where('username=%s', $username)
			 ->fetch();

		if (!$row) {
			throw new AuthenticationException("Neexistující uživatel nebo neplatné heslo.");
		}

		$rolesDibi = $this->connection->select('name')
			 ->from('[' . AclModel::ROLES_TABLE . ']')
			 ->join('[' . AclModel::USERS_ROLES_TABLE . ']')
			 ->using('(id_role)')
			 ->where('id_user=%i',$row->id_user)
			 ->fetchAll();

		$roles = array();
		foreach ($rolesDibi as $role) {
			$roles[] = $role->name;
		}

		// Unseting sensitive data.
		unset($row->password);
		unset($row->hashtype);
		
		return new Identity($row->username, $roles, $row);
	}
}

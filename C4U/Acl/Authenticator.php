<?php

namespace C4U\Acl;

use Nette\Security\IAuthenticator;
use Nette\Security\AuthenticationException;
use Nette\Security\Identity;
use dibi;
use Nette\Security\IIdentity;

/**
 * Description of Authentificator
 *
 */
class Authenticator implements IAuthenticator {

	private $connection;

	/** @var IPasswordValidator $passwordValidator */
	private $passwordValidator;

	public function __construct(
		\Dibi\Connection $dibi,
		IPasswordValidator $passwordValidator
	) {
		$this->connection = $dibi;
		$this->passwordValidator = $passwordValidator;
	}

	/*
	 * Performs an authentication
	 * @param array
	 * @return void
	 * @throws AuthenticationException
	 */
	public function authenticate(array $credentials): IIdentity {
		$username = $credentials[0];
		$password = $credentials[1];

		$row = $this->connection->select('*')
			->from('[' . AclModel::USERS_TABLE . ']')
			->where('username=%s', $username)
			->fetch();

		if (!$row || !$this->passwordValidator->validate($password, $password)) {
			throw new AuthenticationException("Neexistující uživatel nebo neplatné heslo.", self::INVALID_CREDENTIAL);
		}

		$rolesDibi = $this->connection->select('name')
			->from('[' . AclModel::ROLES_TABLE . '] a')
			->join('[' . AclModel::USERS_ROLES_TABLE . '] b')
			->on('a.id_role = b.id_role')
			->where('id_user=%i',$row->id_user)
			->fetchAll();

		$roles = array();
		foreach ($rolesDibi as $role) {
			$roles[] = $role->name;
		}

		// Unseting sensitive data.
		unset($row->password);
		unset($row->hashtype);

		return new Identity($row->username, $roles, $row);
	}

	protected hash
}

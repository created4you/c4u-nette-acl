-- ACL STRUCTURE

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `acl`
-- ----------------------------
DROP TABLE IF EXISTS `acl`;
CREATE TABLE `acl` (
  `id_acl` int(11) NOT NULL AUTO_INCREMENT,
  `allowed` enum('N','Y') COLLATE utf8_general_ci NOT NULL,
  `id_privilege` int(11) NOT NULL,
  `id_resource` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  PRIMARY KEY (`id_acl`),
  KEY `fk_acl_privilege1` (`id_privilege`),
  KEY `fk_acl_resource1` (`id_resource`),
  KEY `fk_acl_role1` (`id_role`),
  CONSTRAINT `fk_acl_privilege1` FOREIGN KEY (`id_privilege`) REFERENCES `privilege` (`id_privilege`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_acl_resource1` FOREIGN KEY (`id_resource`) REFERENCES `resource` (`id_resource`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_acl_role1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
-- ----------------------------
-- Records of company_has_user
-- ----------------------------

-- ----------------------------
-- Table structure for `privilege`
-- ----------------------------
DROP TABLE IF EXISTS `privilege`;
CREATE TABLE `privilege` (
  `id_privilege` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_privilege`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- ----------------------------
-- Table structure for `resource`
-- ----------------------------
DROP TABLE IF EXISTS `resource`;
CREATE TABLE `resource` (
  `id_resource` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_resource`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_general_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `comment` varchar(45) COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_role`),
  KEY `fk_user_role_user_role1` (`parent_id`),
  CONSTRAINT `fk_user_role_user_role1` FOREIGN KEY (`parent_id`) REFERENCES `role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) COLLATE utf8_general_ci DEFAULT NULL,
  `password` varchar(45) COLLATE utf8_general_ci DEFAULT NULL,
  `hashtype` enum('plain','md5','sha1') COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- ----------------------------
-- Table structure for `user_has_role`
-- ----------------------------
DROP TABLE IF EXISTS `user_has_role`;
CREATE TABLE `user_has_role` (
  `id_user_has_role` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  PRIMARY KEY (`id_user_has_role`),
  KEY `fk_user_has_role_user1` (`id_user`),
  KEY `fk_user_has_role_role1` (`id_role`),
  CONSTRAINT `fk_user_has_role_role1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_has_role_user1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

SET FOREIGN_KEY_CHECKS=1;
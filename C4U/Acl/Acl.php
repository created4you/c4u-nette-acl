<?php
namespace C4U\Acl;

use Nette\Security\Permission;

/**
 * Description of Acl
 *
 */
class Acl extends Permission {

	/**
	 * Loads roles, resources and rules from DB and register them.
	 */
	public function __construct(\Dibi\Connection $dibi) {
		$model = AclModel::getInstance($dibi);

		foreach($model->getRoles() as $role) {
			$this->addRole($role['name'], $role['parent_name']);
		}

		foreach($model->getResources() as $resource) {
			$this->addResource($resource->name);
		}

		foreach($model->getRules() as $rule) {
			$this->{$rule->allowed == 'Y' ? 'allow' : 'deny'}($rule->role, $rule->resource, $rule->privilege);
		}
	}

}

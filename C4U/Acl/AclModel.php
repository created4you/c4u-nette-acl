<?php

namespace C4U\Acl;

use dibi;
use Nette\Application\UI\Control;

/**
 * Description of AclModel
 *
 */
class AclModel extends Control {

	static private $instance = null;

	/** @var \DibiConnection $dibi */
	private $dibi;

	// tables
	const ACL_TABLE = 'acl';
	const PRIVILEGES_TABLE = 'acl_privilege';
	const RESOURCES_TABLE = 'acl_resource';
	const ROLES_TABLE = 'acl_role';
	const USERS_TABLE = 'user';
	const USERS_ROLES_TABLE = 'user_has_role';

	// credentials
	const USERNAME = 'username';
	const PASSWORD = 'password';

	// hashtypes
	const HASH_PLAIN = 'plain';
	const HASH_MD5 = 'md5';
	const HASH_SHA1 = 'sha1';


	/**
	 * Return instance of this object
	 *
	 * @return AclModel
	 */
	public static function getInstance(\Dibi\Connection $dibi)
	{
		if (self::$instance === null)
		{
			self::$instance = new self($dibi);
		}

		return self::$instance;
	}

	/**
	 * Constructor
	 */
	protected function __construct(\Dibi\Connection $dibi)
	{
		$this->dibi = $dibi;
	}

	public function getParentRoles($parent_id, $parent_name, &$roles) {
		$sql = $this->dibi->query('SELECT id_role, name
                                FROM [' . self::ROLES_TABLE . ']
                                WHERE %and;', array('parent_id' => $parent_id));
		$rows = $sql->fetchAll();
		if (count($sql)) {
			foreach ($rows as $row) {
				$roles[] = array('name' => $row->name, 'parent_name' => $parent_name);
				$this->getParentRoles($row->id_role, $row->name, $roles);
			}
		}
	}

	/**
	 * Returns id of role with give $name
	 *
	 * @param string $name
	 * @return int
	 */
	public function getRole($name)
	{
		return $this->dibi->query("SELECT id_role FROM %n WHERE name = %n", self::ROLES_TABLE, $name)->fetchSingle();
	}

	/**
	 * Retrives array of defined roles in DB.
	 *
	 * @return array of DibiRow
	 */
	public function getRoles() {
		$roles = array();
	        $this->getParentRoles(NULL, NULL, $roles);
        	return $roles;
	}

	/**
	 * Retrives array of defined resources in DB.
	 *
	 * @return array of DibiRow
	 */
	public function getResources() {
		return $this->dibi->fetchAll('SELECT name FROM ['. self::RESOURCES_TABLE . '] ');
	}

	/**
	 * Retrives array of defined rules in DB.
	 *
	 * @return array of DibiRow
	 */
	public function getRules() {
		return $this->dibi->fetchAll('
            SELECT
                a.allowed as allowed,
                ro.name as role,
                re.name as resource,
                p.name as privilege
                FROM [' . self::ACL_TABLE . '] a
                JOIN [' . self::ROLES_TABLE . '] ro ON (a.id_role = ro.id_role)
                LEFT JOIN [' . self::RESOURCES_TABLE . '] re ON (a.id_resource = re.id_resource)
                LEFT JOIN [' . self::PRIVILEGES_TABLE . '] p ON (a.id_privilege = p.id_privilege)
                ORDER BY a.id_acl ASC
        ');
	}

	/**
	 * Returns hash types
	 *
	 * @return array
	 */
	public static function getHashTypes()
	{
		return array(self::HASH_PLAIN, self::HASH_MD5, self::HASH_SHA1);
	}

	/**
	 * Inserts new user role
	 *
	 * @param array $data
	 * @return int
	 */
	public function insertUserRole(array $data)
	{
		return $this->dibi->insert(self::USERS_ROLES_TABLE, $data)->execute($this->dibi->IDENTIFIER);
	}

	/**
	 * Updates user role
	 *
	 * @param int $id
	 * @param array $data
	 * @return int
	 */
	public function updateUserRole($id, array $data)
	{
		return $this->dibi->update(self::USERS_ROLES_TABLE, $data)->where("id_user = %i", $id)->execute();
	}
}

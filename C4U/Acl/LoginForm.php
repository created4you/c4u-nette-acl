<?php
namespace C4U\Acl;

use Nette\Application\Control;
use Nette\Application\AppForm;
use Nette\Environment;
use Nette\Security\AuthenticationException;

/**
 * LoginForm
 *
 */
class LoginForm extends Control {
	public function createComponentForm($name) {
		$form = new AppForm($this, $name);
		$form->addText(AclModel::USERNAME, "Login");
		$form->addPassword(AclModel::PASSWORD, "Heslo");
		$form->addSubmit("login", "Přihlásit");

		$form->onSubmit[] = array($this, "login");
		
		return $form;
	}

	public function login(AppForm $form) {
		$values = $form->getValues();
		try {
			Environment::getUser()->login($values[AclModel::USERNAME], $values[AclModel::PASSWORD]);
			$this->presenter->flashMessage("Byl jste úspěšně přihlášen", "ok");
			$this->presenter->redirect("this");
		} catch (AuthenticationException $ex) {
			$this->presenter->flashMessage($ex->getMessage(), "error");
			// Sleep one second (brute force attack prevention).
			sleep(1);
		}
	}


	public function render() {
		$this["form"]->getElementPrototype()->class = "ajax";
		$template = $this->createTemplate();
		$template->setFile(dirname(__FILE__) . "/Template/login.phtml");
		$template->render();
	}

}

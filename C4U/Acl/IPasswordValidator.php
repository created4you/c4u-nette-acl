<?php

namespace C4U\Acl;

interface IPasswordValidator {

	public function validate($username, $password);
	
}